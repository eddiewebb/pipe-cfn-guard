# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.5

- patch: modulized python, richer debug

## 0.1.4

- patch: added additonal error handling and debug messages

## 0.1.3

- patch: add cf logo to report

## 0.1.2

- patch: fix docker push process

## 0.1.1

- patch: fix docker push process

## 0.1.0

- minor: basic functionality working

