# Cloud Formation Code Insights for Bitbucket

Uses [AWS's cfn-guard](https://github.com/aws-cloudformation/cloudformation-guard) to scan and report on Cloud Formation Tempate violations


![Adds Code Insights reports to PRs and Pipelines](/assets/cfn-details.png)

![Annotates offending line of template to expediate resolution](/assets/cfn-line.png)

## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| TEMPLATE (*)          | Path to CF Template starting at project root.  |
| RULES (*)             | Path to CF Guard Ruleset starting at project root.|
| SKIP_FAIL             | Don't fail on findings. Default: `false` (build will fail). |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._




## Examples

### Basic example

 upload report and fail on finding
 
```YAML
    - step:
        name: TEST
        script:
          - pipe: docker://eddiewebb/pipe-cfn-guard:0.1.5
            variables:
              TEMPLATE: Examples/policy.json
              RULES: Examples/rules.ruleset
```

### Don't Fail

 upload report and skip failure on finding
 
```YAML
    - step:
        name: TEST
        script:
          - pipe: docker://eddiewebb/pipe-cfn-guard:0.1.5
            variables:
              TEMPLATE: Examples/policy.json
              RULES: Examples/rules.ruleset
              SKIP_FAIL: "True"
```

### Testing

#### build docker and run manually


```
docker run -it -e TEMPLATE="Examples/policy.json" -e RULES="Examples/rules.ruleset" -e DEBUG="true" -e APP_PASSWORD="LOCALTESTOINGONLY" -e BITBUCKET_COMMIT="a747bcf525721600eab0f2d29a704404e3a9142a" -e BITBUCKET_WORKSPACE="webbinaro" -e BITBUCKET_REPO_SLUG="pipe-cfn-guard" -v $(pwd):/working -w /working pipe/test
```


```
APP_PASSWORD="YOURBBAPPPASSWOED" BITBUCKET_COMMIT="6f65f696f27367d27fc220e5dcff8b1eed71a7c0" BITBUCKET_WORKSPACE="eddiewebb" BITBUCKET_REPO_SLUG="pipe-cfn-guard" BITBUCKET_CLONE_DIR=$(pwd)  pytest -v test/test.py
```
