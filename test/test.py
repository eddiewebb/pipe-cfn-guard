import os
import subprocess

docker_image = 'eddiewebb/pipe-cfn-guard:ci' + os.getenv('BITBUCKET_BUILD_NUMBER', 'local')

def docker_build():
  """
  Build the docker image for tests.
  :return:
  """
  args = [
    'docker',
    'build',
    '-t',
    docker_image,
    '.',
  ]
  subprocess.run(args, check=True)


def setup():
  docker_build()

# TODO
# - passing rules
# - fail build
# - > 100 findings


def test_no_parameters():
  args = [
    'docker',
    'run',
    '-v', os.getenv('BITBUCKET_CLONE_DIR') + '/Examples:/working/Examples',
    '-w', '/working',
    docker_image,
  ]

  result = subprocess.run(args, check=False, text=True, capture_output=True)
  assert result.returncode == 1
  assert '✖ Validation errors: \nRULES:\n- required field' in result.stdout

def test_skip_fail():
  args = [
    'docker',
    'run',
    '-e', 'APP_PASSWORD=' + os.getenv('APP_PASSWORD'),
    '-e', 'TEMPLATE="Examples/policy.json"', 
    '-e', 'RULES="Examples/rules.ruleset"', 
    '-e', 'DEBUG=True', 
    '-e', 'BITBUCKET_COMMIT=' + os.getenv('BITBUCKET_COMMIT'), 
    '-e', 'BITBUCKET_WORKSPACE=' + os.getenv('BITBUCKET_WORKSPACE'), 
    '-e', 'BITBUCKET_REPO_SLUG=' + os.getenv('BITBUCKET_REPO_SLUG'), 
    '-e', 'SKIP_FAIL=True', 
    '-v', os.getenv('BITBUCKET_CLONE_DIR') + '/Examples:/working/Examples',
    '-w', '/working',
    docker_image,
  ]

  result = subprocess.run(args, check=False, text=True, capture_output=True)
  assert 'Violations reported, but SKIP_FAIL will let build pass.' in result.stdout
  assert result.returncode == 0


def test_multiple_annotation_batches():
  args = [
    'docker',
    'run',
    '-e', 'APP_PASSWORD=' + os.getenv('APP_PASSWORD'),
    '-e', 'TEMPLATE="Examples/long_policy.json"', 
    '-e', 'RULES="Examples/rules.ruleset"', 
    '-e', 'DEBUG=True', 
    '-e', 'BITBUCKET_COMMIT=' + os.getenv('BITBUCKET_COMMIT'), 
    '-e', 'BITBUCKET_WORKSPACE=' + os.getenv('BITBUCKET_WORKSPACE'), 
    '-e', 'BITBUCKET_REPO_SLUG=' + os.getenv('BITBUCKET_REPO_SLUG'), 
    '-e', 'SKIP_FAIL=True', 
    '-v', os.getenv('BITBUCKET_CLONE_DIR') + '/Examples:/working/Examples',
    '-w', '/working',
    docker_image,
  ]

  result = subprocess.run(args, check=False, text=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT )
  assert 'sending batch 2 of 2' in result.stdout
  assert result.returncode == 0



