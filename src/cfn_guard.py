import os
import subprocess
import re
from bitbucket_pipes_toolkit import Pipe
#CodeInsights from toolkit supports single-annotation requests onlym and weneed batches
#from bitbucket_pipes_toolkit import CodeInsights
from jsoncfg import load_config as load_json
from jsoncfg import node_location 
from jsoncfg.config_classes import ConfigJSONObject, ConfigJSONArray, ConfigJSONScalar



class CfnGuard:


    def __init__(this, pipe ):
        this.report_path = '/tmp/cfn-report'
        this.pipe = pipe

        this.template = this.pipe.get_variable('TEMPLATE')
        this.ruleset = this.pipe.get_variable('RULES')



    def check(this):
        process = subprocess.run(
            [
                'cfn-guard',
                'check',
                '-t', this.template,
                '-r', this.ruleset
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            universal_newlines=True
        )
        if process.returncode == 1: # 2 means violations, 0 means good, 1 means bad arguments
            this.pipe.log_error(process.stderr)
            this.pipe.log_error(process.stdout)
            this.pipe.fail(f'Error running cfn-guard CLI. See previous messages.')
        
        with open(this.report_path, 'w') as f:
            for line in process.stdout:
                f.write(line)
        this.pipe.log_info(f'CLoud guard results saves to {this.report_path}')
        # now take text output mean for humans and parse as annotations
        this.findings = []
        with open(this.report_path, 'r') as fin:
            for cnt, line in enumerate(fin):
                if this.pipe.get_variable('is_debug'):
                    this.pipe.log_debug("processing line: " + line)
                try:
                    finding = this.__match_finding(line)
                    this.findings.append(finding)
                except NoMatchException:
                    continue
                except:
                    this.pipe.log_warning(f'Error processing line: {line}')
                    raise
                     
        this.pipe.log_debug(f'Parsed cfn-guard report and found {len(this.findings)} findings')
        
        return this.findings

    def __match_finding(this,line):
        pattern = re.compile('^\[([0-9A-Za-z]*)\] failed because \[([A-Za-z.]*)\]')
        result = pattern.match(line)
        if not result:
            this.pipe.log_debug('skipping non-finding line')
            raise NoMatchException()
        else:        
            this.pipe.log_debug('Line matches finding format')
            # magic string, 
            path = f'Resources.{result[1]}.Properties.{result[2]}'
            node = this.__node_at_path(path)
            message = line
            return {
                "file_path": this.template,
                "node_path": path,
                "line": node_location(node).line,
                "message": message
            }
       

    """
    Uses a dotted path 'TOp.Next.Next.Child' to return the actual node from nested dict
    """
    def __node_at_path(this, dotted_path):

        this.parsed = load_json(this.template)
        dct = this.parsed
        for key in dotted_path.split('.'):
            try:
                dct = dct[key]
            except KeyError:
                pass
        return dct

class NoMatchException(Exception):
    def __init__(this):
        pass