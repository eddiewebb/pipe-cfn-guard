import os
import subprocess
import re
import uuid
import requests
from bitbucket_pipes_toolkit import Pipe
#CodeInsights from toolkit supports single-annotation requests onlym and weneed batches
#from bitbucket_pipes_toolkit import CodeInsights
from jsoncfg import load_config as load_json
from jsoncfg import node_location 
from jsoncfg.config_classes import ConfigJSONObject, ConfigJSONArray, ConfigJSONScalar
from cfn_guard import CfnGuard
from insights_client import BitbucketCodeInsightsClient as BBClient

os.system('ls -la')

variables = {
  'TEMPLATE': {'type': 'string', 'required': True},
  'RULES': {'type': 'string', 'required': True},
  'SKIP_FAIL': {'type': 'boolean', 'required': False, 'default': False},
  'DEBUG': {'type': 'boolean', 'required': False, 'default': False},
  'APP_PASSWORD': {'type': 'string', 'required': False}
}
pipe = Pipe(schema=variables)


pipe.log_info("Executing the cfn-guard check...")
cfnGuard = CfnGuard(pipe)
findings = cfnGuard.check()


pipe.log_info("Sending report to Code Insights")
client = BBClient(pipe)
client.send_report(findings)


pipe.log_info("Deciding build outcome...")
if len(findings) == 0: 
    pipe.success(message="Success!")
elif pipe.get_variable('SKIP_FAIL'):
    pipe.success(message="Violations reported, but SKIP_FAIL will let build pass.")
else:
    pipe.fail(message="There were cfn-guard violations in the template. See report. You can set SKIP_FAIL in your bitbucket-pipelines.yml to allow build to pass.")