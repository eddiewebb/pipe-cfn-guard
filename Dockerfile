FROM python:3.7-slim

RUN apt update -y && apt install -y wget

RUN wget -q https://github.com/aws-cloudformation/cloudformation-guard/releases/download/1.0.0/cfn-guard-linux-1.0.0.tar.gz \
	&& tar -xvf cfn-guard-linux-1.0.0.tar.gz \
	&& mv ./cfn-guard-linux/cfn-guard /usr/local/bin/ \
	&& rm -R ./cfn-guard-linux*



COPY requirements.txt /
RUN pip install -r requirements.txt

COPY pipe.py src /
ENV PYTHONPATH="/src"

ENTRYPOINT ["python3", "/pipe.py"]
